<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

//Class to add custom scripts and styles
if( !class_exists( 'WCBPTBScriptsStyles' ) ) {
    class WCBPTBScriptsStyles{

        //Constructor
        function __construct() {
            $curr_url = $_SERVER['REQUEST_URI'];
            if( strpos($curr_url, 'wcbptb-setting-options') !== false ) {
                add_action( 'admin_enqueue_scripts', array( $this, 'wcbptb_admin_variables' ) );
            }
        }

        //Actions performed for enqueuing scripts and styles for admin panel
        function wcbptb_admin_variables() {
            wp_enqueue_style('wcbptb-admin-css', WCBPTB_PLUGIN_URL.'admin/assets/css/wcbptb-admin.css');
        }
    }
    new WCBPTBScriptsStyles();
}