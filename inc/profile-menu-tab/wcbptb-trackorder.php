<?php
// if this file is called directly abort
if ( ! defined( 'ABSPATH' ) ) exit;
global $woocommerce;
// it will produce code output for track order
echo do_shortcode('[woocommerce_order_tracking]');