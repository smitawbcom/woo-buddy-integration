<?php
// if this file is called directly abort
if ( ! defined( 'ABSPATH' ) ) exit;

function wcbptb_member_profile_shop_tab(){
	global $bp;
	$name = bp_get_displayed_user_username();
	//Admin Settings
	$off_tabs = get_option( 'woo_buddy_tabs_off');
	if( strlen( $off_tabs ) != 0 ) {
		$off_tabs = unserialize( $off_tabs );
	}

	$tab_args = array(
		'name' => 'My Account',
		'slug' => 'my-account',
		'screen_function' => 'my_profile_shop_tab_function_to_show_screen',
		'position' => 75,
		'default_subnav_slug' => 'my-account-sub',
		'show_for_displayed_user' => true,
	);
	bp_core_new_nav_item( $tab_args );

	$parent_slug = 'my-account';

	if( empty( $off_tabs ) || !in_array( 'cart', $off_tabs ) ) {
		//Add subnav shoppingcart
		bp_core_new_subnav_item(
			array(
				'name' => 'ShoppingCart',
				'slug' => 'shopping-cart',
				'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
				'parent_slug' => $parent_slug,
				'screen_function' => 'wcbptb_shoppingcart_show_screen',
				'position' => 100,
				'link' => site_url()."/members/$name/$parent_slug/shopping-cart/",
			)
		);
	}

	if( empty( $off_tabs ) || !in_array( 'track_order', $off_tabs ) ) {
		//Add subnav Track Your Order
		bp_core_new_subnav_item(
			array(
				'name' => ' Track Your Order',
				'slug' => 'track-order',
				'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
				'parent_slug' => $parent_slug,
				'screen_function' => 'wcbptb_trackorder_show_screen',
				'position' => 100,
				'link' => site_url()."/members/$name/$parent_slug/track-order/",
			)
		);
	}

	if( empty( $off_tabs ) || !in_array( 'pur_history', $off_tabs ) ) {
		//Add subnav to Show History
		bp_core_new_subnav_item(
			array(
				'name' => 'Purchase History',
				'slug' => 'history',
				'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
				'parent_slug' => $parent_slug,
				'screen_function' => 'wcbptb_history_show_screen',
				'position' => 100,
				'link' => site_url()."/members/$name/$parent_slug/history/",
			)
		);
	}

	if( empty( $off_tabs ) || !in_array( 'my_downloads', $off_tabs ) ) {
		//Add subnav to Show My Downloads
		bp_core_new_subnav_item(
			array(
				'name' => 'My Downloads',
				'slug' => 'my-downloads',
				'parent_url' => $bp->loggedin_user->domain . $parent_slug.'/',
				'parent_slug' => $parent_slug,
				'screen_function' => 'wcbptb_mydownloads_show_screen',
				'position' => 100,
				'link' => site_url()."/members/$name/$parent_slug/my-downloads/",
			)
		);
	}
}
add_action( 'bp_setup_nav', 'wcbptb_member_profile_shop_tab', 301 );

//Screen functions for "shop tab"
function my_profile_shop_tab_function_to_show_screen() {
	add_action( 'bp_template_title', 'wcbptb_shop_tab_function_to_show_title' );
	add_action( 'bp_template_content', 'wcbptb_shop_tab_function_to_show_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function wcbptb_shop_tab_function_to_show_title(){
	echo 'My Account Details';
}

function wcbptb_shop_tab_function_to_show_content(){
	include 'profile-menu-tab/wcbptb-myaccount.php';
}

//Screen functions for "shopping cart tab"
function wcbptb_shoppingcart_show_screen(){
	add_action( 'bp_template_title', 'wcbptb_shoppingcart_tab_function_to_show_title' );
	add_action( 'bp_template_content', 'wcbptb_shoppingcart_tab_function_to_show_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function wcbptb_shoppingcart_tab_function_to_show_title(){
	echo 'shopping cart';
}

function wcbptb_shoppingcart_tab_function_to_show_content(){
	include 'profile-menu-tab/wcbptb-cart.php';
}

//Screen functions for "Track-order tab"
function wcbptb_trackorder_show_screen(){
	add_action( 'bp_template_title', 'wcbptb_trackorder_tab_function_to_show_title' );
	add_action( 'bp_template_content', 'wcbptb_trackorder_tab_function_to_show_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function wcbptb_trackorder_tab_function_to_show_title(){
	echo 'Track order title';
}

function wcbptb_trackorder_tab_function_to_show_content(){
	include 'profile-menu-tab/wcbptb-trackorder.php';

}


//Screen functions for "History tab"
function wcbptb_history_show_screen(){
	add_action( 'bp_template_title', 'wcbptb_history_tab_function_to_show_title' );
	add_action( 'bp_template_content', 'wcbptb_history_tab_function_to_show_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function wcbptb_history_tab_function_to_show_title(){
	echo 'Product Order Details';
}

function wcbptb_history_tab_function_to_show_content(){
	include 'profile-menu-tab/wcbptb-purchase-history.php';
}

//Screen functions for "My Downloads tab"
function wcbptb_mydownloads_show_screen(){
	add_action( 'bp_template_title', 'wcbptb_mydownloads_tab_function_to_show_title' );
	add_action( 'bp_template_content', 'wcbptb_mydownloads_tab_function_to_show_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function wcbptb_mydownloads_tab_function_to_show_title(){
	echo 'My Downloads';
}

function wcbptb_mydownloads_tab_function_to_show_content(){
	include 'profile-menu-tab/wcbptb-mydownloads.php';
}