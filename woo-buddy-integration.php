<?php
/*
Plugin Name: WooCommerce Buddypress Integration
Plugin URI: https://wbcomdesigns.com/contact/
Description: This plugin will add specific tabs of woocommerce my account page to buddypress members profile page.
Version: 1.0.0
Author: Wbcom Designs
Author URI: http://wbcomdesigns.com
Text Domain: wc-bp-tabs
*/

// If this file is called directly abort
if ( ! defined( 'ABSPATH' ) ) exit;

//Defining constants
$cons = array(
	'WCBPTB_PLUGIN_PATH' => plugin_dir_path(__FILE__),
	'WCBPTB_PLUGIN_URL' => plugin_dir_url(__FILE__),
);
foreach( $cons as $con => $value ) {
	define( $con, $value);
}

//Include needed files
$include_files = array(
	'inc/wcbptb-scripts.php',
	'inc/wcbptb-profile-tab.php',
	'admin/wcbptb-admin.php',
);
foreach( $include_files  as $include_file ) {
	include $include_file;
}

//Woocommerce Buddypress Plugin Activation
register_activation_hook( __FILE__, 'wcbptb_plugin_activation' );
function wcbptb_plugin_activation() {
	//Check if "Buddypress" plugin is active or not
	if (!in_array('buddypress/bp-loader.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		//Buddypress Plugin is inactive, hence deactivate this plugin
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die( __( 'The <b>WC Buddypress Integration</b> plugin requires <b>Buddypress</b> plugin to be installed and active. Return to <a href="'.admin_url('plugins.php').'">Plugins</a>','wc-bp-tabs' ));
	}

	//Check if "Woocommerce" plugin is active or not
	if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		//Woocommerce Plugin is inactive, hence deactivate this plugin
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die( __( 'The <b>WC buddy integration</b> plugin requires <b>Woocommerce</b> plugin to be installed and active. Return to <a href="'.admin_url('plugins.php').'">Plugins</a>', 'wc-bp-tabs' ) );
	}
}

register_deactivation_hook( __FILE__, 'wcbptb_plugin_deactivation' );
function wcbptb_plugin_deactivation() {

}

//Define setting links
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'wcbptb_setting_page_links');
function wcbptb_setting_page_links($links) {
	$page_link = array('<a href="' . admin_url('admin.php?page=wcbptb-setting-options') . '">Settings</a>');
	return array_merge($links, $page_link);
}

//function for displaying purchase order history
// Add shortcode in profile-menu-tab/wcbptb-purchase-history.php file
function shortcode_my_orders( $atts ) {
	
   $extract =  extract( shortcode_atts( array(
        'order_count' => -1
    ), $atts ) );

    ob_start();
     wc_get_template( 'myaccount/my-orders.php', array(
        'current_user'  => get_user_by( 'id', get_current_user_id() ),
        'order_count'   => $order_count
    ) );

    return ob_get_clean();
}
add_shortcode('woocommerce_view_order', 'shortcode_my_orders');

//Function for displaying my-downloads 
// Add shortcode in profile-menu-tab/wcbptb-mydownloads.php
function shortcode_product_downloads() {
	ob_start();
	wc_get_template( 'myaccount/my-downloads.php', array(
		'current_user' => get_user_by( 'id', get_current_user_id() )
		) );
	return ob_get_clean();
}
add_shortcode('woocommerce_my_downloads', 'shortcode_product_downloads');