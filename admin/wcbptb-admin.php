<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Add admin page for woocommerce buddypress integration
if( !class_exists( 'WCBPTBAdmin' ) ) {
    class WCBPTBAdmin{

        //Constructor
        function __construct() {
            add_action( 'admin_menu', array( $this, 'wcbptb_admin_page' ) );
        }

        //Actions performed on loading admin_menu
        function wcbptb_admin_page() {
            add_menu_page( 'WooCommerce Buddypress Integration', 'WC-BP Integration', 'manage_options', 'wcbptb-setting-options', array( $this, 'wcbptb_options_page' ), 'dashicons-admin-tools', 5 );
        }

        function wcbptb_options_page() {
            include 'wcbptb-admin-optionspage.php';
        }
    }
    new WCBPTBAdmin();
}