<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Save Settings

if(isset($_POST['submittab']) && wp_verify_nonce($_POST['submittab_nonce'],'wc-tabs')) {
	if( isset( $_POST['tab'] ) ) {
		$tabs_to_remove = $_POST['tab'];
		update_option('woo_buddy_tabs_off', serialize( $tabs_to_remove ) );
	} else {
		update_option('woo_buddy_tabs_off', '' );
	}
	?>
	<div class="notice notice-success"><p><?php _e('Settings Saved.','wc-bp-tabs');?></p></div>
<?php 
}
$off_tabs = get_option( 'woo_buddy_tabs_off' );
if( strlen( $off_tabs ) != 0 ) {
	$off_tabs = unserialize( $off_tabs );
}

?>
<div id="wpbody" role="main">
	<div id="wpbody-content" aria-label="Main content" tabindex="0">
		<div class="wrap">
			<div class="plugin-logos">
				<div class="woo-logo">
					<img src="<?php echo WCBPTB_PLUGIN_URL.'admin/assets/images/woocommerce-logo.png';?>">
				</div>
				<div class="bp-logo">
					<img src="<?php echo WCBPTB_PLUGIN_URL.'admin/assets/images/buddypress_logo.png';?>">
				</div>
			</div>
			<h1><?php _e( 'Integration of WooCommerce with Buddypress!', 'wc-bp-tabs' );?></h1>
			<p><?php _e( 'You can turn off any tabs on buddypress members profiles by mark it checked and save.', 'wc-bp-tabs' );?></p>
			<form method="post" action="">
				<table class="form-table">
					<tbody>
						<tr class="">
							<th scope="row"><?php _e( 'Turn Off Woo Tabs', 'wc-bp-tabs' );?></th>
							<td>
								<fieldset>
									<legend class="screen-reader-text">
										<span>Search Engine Visibility</span>
									</legend>
									<label for="shopping_cart">
										<input name="tab[]" value="cart" <?php if( !empty( $off_tabs ) && in_array( 'cart', $off_tabs ) ) echo "checked='checked'";?> type="checkbox"><?php _e( 'Shopping Cart Tab', 'wc-bp-tabs' );?>
									</label><br>
									<label for="history_tab">
										<input name="tab[]" value="pur_history" <?php if( !empty( $off_tabs ) && in_array( 'pur_history', $off_tabs ) ) echo "checked='checked'";?> type="checkbox"><?php _e( 'Purchase History Tab', 'wc-bp-tabs' );?>
									</label><br>
									<label for="track_order">
										<input name="tab[]" value="track_order" <?php if( !empty( $off_tabs ) && in_array( 'track_order', $off_tabs ) ) echo "checked='checked'";?> type="checkbox"><?php _e( 'Track My Order Tab', 'wc-bp-tabs' );?>
									</label><br>
									<label for="my_downloads">
										<input name="tab[]" value="my_downloads" <?php if( !empty( $off_tabs ) && in_array( 'my_downloads', $off_tabs ) ) echo "checked='checked'";?> type="checkbox"><?php _e( 'My Downloads Tab', 'wc-bp-tabs' );?>
									</label><br>
								</fieldset>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
				<?php wp_nonce_field('wc-tabs','submittab_nonce');?>
					<input name="submittab" class="button button-primary" value="<?php _e( 'Save Changes', 'wc-bp-tabs' );?>" type="submit">
				</p>
			</form>
		</div>
		<div class="clear"></div>
	</div><!-- wpbody-content -->
	<div class="clear"></div>
</div>