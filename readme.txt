=== WC BP Integration ===
Contributors: vapvarun,wbcomdesigns
Tags: buddypress,Woocommerce, My Account Tabs
Donate link: https://wbcomdesigns.com/donate/
Requires at least: 4.6
Tested up to: 4.6.1
Stable tag: 4.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin allow you to turn off woocommerce tabs on buddypress member profile.

== Description ==
This plugin adds a new feature to buddypress member profile to integrate woocommerce my account tabs with buddypress member profile.
If you need additional help you can contact us for [Custom Development](https://wbcomdesigns.com/contact/).


== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the \'Plugins\' screen in WordPress
3. Use the WC-BP Integration to turn off woocommerce tabs on buddypress profiles.


== Frequently Asked Questions ==
= Will it work with BP2.6 lower version =
No

== Screenshots ==

1. Woocommerce Buddypress Integration - WC-BP Integration settings

== Changelog ==
= 1.0.0 =
Initial Release